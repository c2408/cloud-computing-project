import os, time, string, random, argparse, pika, threading, base64
from flask import *
from google.cloud import storage
from waitress import serve
from subprocess import Popen, PIPE

# Version 1.4
# Built on 4/1/22
# -------------- Latest Updates: -----------------
# Updated webServer to use rabbitmq for communica-
# tion between CNN app. Also added text to index.-
# html for CNN prediction
# ------------------------------------------------

#Credits:
# - Flask official documentation https://flask.palletsprojects.com/en/2.0.x/
# - Random string generation https://www.educative.io/edpresso/how-to-generate-a-random-string-in-python
# - Google Cloud documentation https://cloud.google.com/appengine/docs/flexible/python/using-cloud-storage

#Setup allowed variable
global dialog
global cnnText

# setup the rabbitmq credentials
print("Configuring rabbitmq connection info...")
rabbitmq_host = os.environ["RABBITMQ_1_RABBITMQ_SVC_SERVICE_HOST"]
rabbit_pass = os.environ["RABBITMQ_SECRET"]
#rabbit_pass = base64.b64decode(rabbit_pass).decode()
credentials = pika.PlainCredentials("rabbit", rabbit_pass)

#Set up Flask, argument parser 
webServer = Flask(__name__, template_folder = "web")
parser = argparse.ArgumentParser(description="Arguments for webServer")

#Set up google cloud storage, replace cloud.json with secret from kubernetes
os.system("base64 -d /etc/gcp/gbot_credentials.json > gbot_credentials64.json")
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "gbot_credentials64.json"

#Set up storage client
gCloudClient = storage.Client()
gBucket = gCloudClient.get_bucket("artifacts.cloud-computing-project-341721.appspot.com")

def callback(ch, method, properties, body):
    global cnnText

    cnnText = str(body)
    cnnText = cnnText.replace("b", "").replace("'", "")
    cnnText = "I guess it is a " + cnnText
    
    if(body == b'dog' or body == b'cat'):
       print("Received %r" % body)
    else:
       print("Bad recv\n")
    ch.close()

def cnnRecv():
    connection = pika.BlockingConnection(pika.ConnectionParameters(rabbitmq_host, 5672, '/', credentials))
    channel = connection.channel()
    channel.queue_declare(queue = "web", durable = True)
    channel.queue_declare(queue = "cnn", durable = True)
    channel.basic_consume(queue='cnn', on_message_callback=callback, auto_ack=True)
    print("Waiting for message...\n")
    consumeThread = threading.Thread(target = channel.start_consuming())
    consumeThread.start()

def arguments():
    parser.add_argument("-ip", "--hostname", help="Defines what IP address to use for the web app", default = "127.0.0.1")
    parser.add_argument("-p", "--port", help="Defines what port to use for the web app", default = "5000")
    args = parser.parse_args()
    return args

def rabbitSend(url):
    connection = pika.BlockingConnection(pika.ConnectionParameters(rabbitmq_host, 5672, '/', credentials))
    channel = connection.channel()
    channel.queue_declare(queue = "web", durable = True)
    channel.queue_declare(queue = "cnn", durable = True)
    print("Sending URL\n")
    channel.basic_publish(exchange='', routing_key='web', body=url)
    channel.close()

    cnnRecv()

def main():
    #Parse arguments
    args = arguments()

    #Configure flask and it's settings such as upload path and allowed extensions
    configureFlask()

    #Serve the webServer using waitress
    print("Serving web server...")
    serve(webServer, host = args.hostname, port = args.port, threads=4)

    #Redirect user to index.html
    home()


def configureFlask():
    print("Configuring Flask...")
    webServer.config["UPLOAD_FOLDER"] = "uploads" #Folder to upload files (locally)
    webServer.config["UPLOAD_EXTENSION"] = ("bmp", "jpg", "gif", "png") #Accepted file extensions
    webServer.config["MAX_CONTENT_LENGTH"] = 32000000 #Maximum filesize is 32 megabytes
    return webServer

@webServer.route("/")
def home():
    with webServer.app_context():
        return render_template("index.html")

@webServer.route("/", methods = ["POST"])
def uploadToG():
    #Variables
    finalPhotoName = ""
    global cnnText

    #First we must get the photo from index.html, we can find this under photo_file, defined in index.html
    uploadedPhoto = request.files.get("photo_file")

    #Get photo's filename and replace spaces with dashes
    photoName = uploadedPhoto.filename.replace(" ", "-")

    #Find photos extension, is it a png? gif?
    photoExtension = os.path.splitext(photoName)[1]
    
    #If it is, accept it; otherwise display error
    if photoExtension.lower() in [".bmp", ".jpg", ".gif", ".png"]: 
        print(photoExtension)
    else:
        print("Bad Input!\n")
        dialog = "Bad Input -- Rejected file: {}!".format(photoName)
        with webServer.app_context():
            return render_template("index.html", allowed = dialog)

    #Does this file or a file with the same name already exist on the cloud storage?
    #We want a unique filename
    finalPhotoName = checkFilesG(photoName, photoExtension)
    print("Final Photo Name: ", finalPhotoName)

    #Local storage upload
    #uploadedPhoto.save(os.path.join(webServer.config["UPLOAD_FOLDER"], finalPhotoName))

    #Save file to cloud storage
    gBlob = gBucket.blob("uploads/" + finalPhotoName)
    gBlob.upload_from_string(uploadedPhoto.read(), content_type=uploadedPhoto.content_type)
    dialog = "File Uploaded Successfully\n"
    url = "https://storage.googleapis.com/artifacts.cloud-computing-project-341721.appspot.com/uploads/" + finalPhotoName

    #Send URL to CNN app
    rabbitSend(url)

    with webServer.app_context():
        return render_template("index.html", allowed = dialog, image = url, cnn = cnnText)

def checkFilesG(photoName, photoExtension):
    print("Checking filename ...\n")
    #Variables
    tempPhotoName = photoName

    while(True):

         ls = storage.Blob(bucket=gBucket, name= "uploads/" + tempPhotoName).exists(gCloudClient)

         if ls:
             print("Yes")

             #If this filename exists, add 5 random characters to photo name
             tempPhotoName = tempPhotoName.split(".") 
             tempPhotoName = tempPhotoName[0]
             #Don't exceed character limit for 32
             if len(tempPhotoName) < 32:
                tempPhotoName += random.choice(string.ascii_lowercase)
             else:
                tempPhotoName = random.choice(string.ascii_lowercase)
             tempPhotoName += photoExtension

             #Check if new name is also taken, if so, re-run this
             print("Run it again...")
         else:
             #Perfect!
             print("No")
             print("being sent back:")
             break

    return tempPhotoName


def checkFilesLocal(photoName, photoExtension):
    print("Checking filename ...\n")
    #Variables
    ls = ""
    tempPhotoName = photoName

    while(True):

      with webServer.app_context():
         ls = os.listdir(webServer.config["UPLOAD_FOLDER"])

         if tempPhotoName in ls:
             print("Yes")

             #If this filename exists, add 5 random characters to photo name
             tempPhotoName = tempPhotoName.split(".") 
             tempPhotoName = tempPhotoName[0]
             #Don't exceed character limit for 32
             if len(tempPhotoName) < 32:
                tempPhotoName += random.choice(string.ascii_lowercase)
             else:
                tempPhotoName = random.choice(string.ascii_lowercase)
             tempPhotoName += photoExtension

             #Check if new name is also taken, if so, re-run this
             print("Run it again...")
         else:
             #Perfect!
             print("No")
             print("being sent back:")
             break

    return tempPhotoName
            

if __name__ == "__main__":
    main()

