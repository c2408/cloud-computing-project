# CS-7346 Cloud Computing Project
## Contributors 
William Sizemore
<br>
Ishna Satyarth
<br>
Nick Dejulia

# Features of this project
## 1. Image uploader web page using python flask
- Using python flask, runs a web page page that allows the user to upload an image using the interface

- Does checks against the file type to ensure its an acceptable format

- Does checks to see if the file name already exists, and if so, then it appends randomize characters to the end of the filename

- Utilizes the GCP storage library to interact with the cloud storage

- running from a container in kubernetes

- Sends message over the RabbitMQ broker to trigger the image scan

## 2. Message Broker using RabbitMQ
- Containerized and running in cluster deployment

- Used to allow communication between the flask site and image recognition containers

## 2. Image recognition in python using tensorflow
- Using tensorflow in python to learn images based on provided datasets

- Listens to message broker for images it needs to scan

- Scans images stored in the cloud storage according to the provided message 

- Sends results back on RabbitMQ to be displayed in the webapp 


## 3. CI/CD pipeline to containerize and push the images to GCP registry
- Uses GitLab's built-in CI/CD pipeline capabilities and the available shared runners they offer for free (just with some user verification needed).

- Uses a single dockerfile that is fully modular and feeds it the variables necessary for the individual service containerization

- Uses a base job interface/parent which is implemented by the service jobs that *extend* it with their specifications

- tags the commit sha and latest for each push to maintain some history while still keeping a single source to point to

## 4. Kubernetes cluster
- Autoscaling cluster (3-5 nodes) hosting deployments for the following: 
1. Flask site
2. RabbitMQ
3. image recognition (convolutionalnn)
4. monitoring suite:
    * istio 
    * prometheus
    * grafana
    * jaeger tracing
    * kiali
- Healthchecks configured for nodes in the cluster
- Horizonal Pod Autoscaling (HPA) configured for our services
- use of persistent volumes separate from nodes for services that need them 

