from tensorflow.keras.models import model_from_json
import cv2
import numpy as np
from tensorflow.keras.preprocessing.image import load_img
import pika
import os, base64
from google.cloud import storage
import urllib.request

#Set up google cloud storage, replace cloud.json with secret from kubernetes
os.system("base64 -d /etc/gcp/gbot_credentials.json > gbot_credentials64.json")
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "gbot_credentials64.json"



#Set up storage client
gCloudClient = storage.Client()
gBucket = gCloudClient.get_bucket("artifacts.cloud-computing-project-341721.appspot.com")

# setup connection to rabbitmq broker
rabbitmq_host= os.environ["RABBITMQ_1_RABBITMQ_SVC_SERVICE_HOST"]

# setup the rabbitmq credentials
print("Configuring rabbitmq connection info...")
rabbitmq_host = os.environ["RABBITMQ_1_RABBITMQ_SVC_SERVICE_HOST"]
rabbit_pass = os.environ["RABBITMQ_SECRET"]
#rabbit_pass = base64.b64decode(rabbit_pass).decode()
credentials = pika.PlainCredentials("rabbit", rabbit_pass)

# setup amq connections
connection = pika.BlockingConnection(pika.ConnectionParameters(rabbitmq_host, 5672, '/', credentials))
channel = connection.channel()
channel.queue_declare(queue="web", durable=True)
channel.queue_declare(queue="cnn", durable=True)


def callback(ch, method, properties, body):
    print("Recieved message: %r" % body.decode())

    # Step 2: Load the Model from Json File
    json_file = open('model.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)

    # Step 3: Load the weights
    loaded_model.load_weights("./model.h5")
    print("Loaded model from disk")

    # Step 4: Compile the model
    loaded_model.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])

    # take the image url and read it in
    img_link = urllib.request.urlopen(body.decode())
    image_proc = np.asarray(bytearray(img_link.read()), dtype="uint8")
    image_proc = cv2.imdecode(image_proc, cv2.IMREAD_COLOR)

    # Step 5: load the image you want to test
    image = cv2.resize(image_proc, (50,50))
    image = image.reshape(1, 50, 50, 3)

    # Step 6: Predict to which class your input image has been classified
    result = loaded_model.predict_classes(image)
    guess = "myguess"
    if(result[0][0] == 1):
        print("I guess this must be a Dog!")
        guess = "dog"
    else:
        print("I guess this must be a Cat!")
        guess = "cat"

    channel.basic_publish(exchange='',routing_key='cnn', body=guess)
    print('Sent result of ' + guess)


channel.basic_consume(queue="web", on_message_callback=callback, auto_ack=True)                         

print("Waiting for message...")
channel.start_consuming()

