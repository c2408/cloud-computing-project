ARG BASE_IMAGE_ARG
FROM $BASE_IMAGE_ARG

ARG SERVICE_ARG
ARG START_ARG
ARG INSTALL_ARG1
ARG INSTALL_ARG2

# specific service we want to use
COPY $SERVICE_ARG/ /root/

WORKDIR /root/

# command to install needed dependencies
RUN yum install pip -y
RUN $INSTALL_ARG1
RUN $INSTALL_ARG2

ENV START_CMD ${START_ARG}
# start up the service for the container. This may need to also update the entrypoint 
CMD [ "sh", "-c", "${START_CMD}" ]

